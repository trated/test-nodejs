import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import cors from 'cors'
import { route as UserRoute } from './usersRoute'

connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})
app.use('/users', UserRoute)

app.listen(8080, () => console.log('Example app listening on port 8080!'))
