import { Router } from 'express'
const route = Router()
import UserModel from './UserModel'

route.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

route.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

route.put('/:id', async (req, res, nex) => {
  const { id } = req.params
  const update = req.body
  try {
    const updateUser = await UserModel.findByIdAndUpdate()
    if (!updateUser) res.status(400).send('User not found')
    res.status(200).send('User is updated')
  } catch (error) {
    //Handle error with middleware :
    // error.message = "Error while updating the user"
    //error.httpStatus = 500
    // nex(error)
    res.status(500).send('Generic Error')
  }
})

route.delete('/:id', async (req, res, nex) => {
  const { id } = req.params
  try {
    const deleteUser = await UserModel.findByIdAndDelete(id)
    if (!deleteUser) res.status(404).send('User not found')
    res.status(200).send('User is deleted')
  } catch (error) {
    //Handle error with middleware :
    // error.message = "Error while updating the user"
    //error.httpStatus = 500
    // nex(error)
    res.status(500).send('Generic Error')
  }
})
